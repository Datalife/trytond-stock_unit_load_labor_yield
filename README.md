datalife_stock_unit_load_labor_yield
====================================

The stock_unit_load_labor_yield module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-stock_unit_load_labor_yield/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-stock_unit_load_labor_yield)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
