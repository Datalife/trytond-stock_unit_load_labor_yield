# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from .labor_yield import YieldAllocation


def register():
    Pool.register(
        YieldAllocation,
        module='stock_unit_load_labor_yield', type_='model')