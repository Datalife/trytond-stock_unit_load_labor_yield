# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta
from trytond.transaction import Transaction
from trytond.pool import Pool


class YieldAllocation(metaclass=PoolMeta):
    __name__ = 'labor.yield.allocation'

    unit_load = fields.Many2One(
        'stock.unit_load', 'Unit load',
        select=True, ondelete='RESTRICT', readonly=True)

    @classmethod
    def _get_source_name(cls):
        result = super(YieldAllocation, cls)._get_source_name()
        result.append('stock.unit_load')
        return result

    @classmethod
    def view_attributes(cls):
        attrs = super(YieldAllocation, cls).view_attributes()
        pool = Pool()
        work_id = Transaction().context.get('work', None)
        if work_id:
            Work = pool.get('timesheet.work')
            work = Work(work_id)
            if (work.yield_available
                    and work.yield_record_granularity == 'company'):
                attrs += [
                    ('/tree//field[@name="unit_load"]',
                     'tree_invisible', 1)]
        return attrs
