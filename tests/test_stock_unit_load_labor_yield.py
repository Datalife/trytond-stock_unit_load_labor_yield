# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
import unittest

from trytond.tests.test_tryton import ModuleTestCase
from trytond.tests.test_tryton import suite as test_suite


class StockUnitLoadLaborYieldTestCase(ModuleTestCase):
    """Test Stock Unit Load Labor Yield module"""
    module = 'stock_unit_load_labor_yield'


def suite():
    suite = test_suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(
            StockUnitLoadLaborYieldTestCase))
    return suite
